import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { takeWhile } from 'rxjs/operators';
import { Area } from 'src/app/core/entities/area';
import { Pais } from 'src/app/core/entities/pais';
import { TipoIdentificacion } from 'src/app/core/entities/tipoIdentificacion';
import { AreasService } from 'src/app/core/services/areas/areas.service';
import { EmpleadosService } from 'src/app/core/services/empleados/empleados.service';
import { PaisesService } from 'src/app/core/services/paises/paises.service';
import { TiposIdentificacionService } from 'src/app/core/services/tiposIdentificacion/tipos-identificacion.service';

@Component({
  selector: 'app-edicion',
  templateUrl: './edicion.component.html',
  styleUrls: ['./edicion.component.scss']
})
export class EdicionComponent implements OnInit, OnDestroy {

  alive = true;
  edicionForm: FormGroup;
  areas: Area[] = [];
  tiposIdentificacion: TipoIdentificacion[] = [];
  paises: Pais[] = [];
  empleadoID = 0;

  fechaRegistroFilter = (date: Date | null): boolean => {
    date = date || new Date();
    const now = new Date();
    const difference = now.valueOf() - date.valueOf();
    return difference > 0 && difference < 31 * 24 * 60 * 60 * 1000;
  }

  constructor(
    private formBuilder: FormBuilder,
    private areasService: AreasService,
    private empleadosService: EmpleadosService,
    private paisesService: PaisesService,
    private tiposIdentificacionService: TiposIdentificacionService,
    private router: Router,
    private route: ActivatedRoute,
  ) {
    this.edicionForm = this.formBuilder.group({
      primerNombre: ['', [Validators.required]],
      otrosNombres: ['', []],
      primerApellido: ['', [Validators.required]],
      segundoApellido: ['', [Validators.required]],
      fk_idPais: ['', [Validators.required]],
      fk_idArea: ['', [Validators.required]],
      fk_idTipoIdentificacion: ['', [Validators.required]],
      numeroIdentificacion: ['', [Validators.required]],
      // fechaDeIngresoTimestamp: [null, [Validators.required]],
    });
  }

  ngOnInit(): void {
    this.retrieveEmpleado();
    this.retrieveAreas();
    this.retrievePaises();
    this.retrieveTiposIdentificacion();
  }

  retrieveEmpleado(): void {
    this.route.paramMap.subscribe(params => {
      this.empleadoID = +(params.get('idEmpleado') || '0');
      this.empleadosService.getEmpleado(this.empleadoID)
        .pipe(takeWhile(() => this.alive))
        .subscribe((empleado) => {
          this.edicionForm.patchValue(empleado);
      });
    });
  }

  retrieveAreas(): void {
    this.areasService.getAreas()
      .pipe(takeWhile(() => this.alive))
      .subscribe((areas) => {
        this.areas = areas;
    });
  }

  retrievePaises(): void {
    this.paisesService.getPaises()
      .pipe(takeWhile(() => this.alive))
      .subscribe((paises) => {
        this.paises = paises;
    });
  }

  retrieveTiposIdentificacion(): void {
    this.tiposIdentificacionService.getTiposIdentificacion()
      .pipe(takeWhile(() => this.alive))
      .subscribe((tiposIdentificacion) => {
        this.tiposIdentificacion = tiposIdentificacion;
    });
  }

  updateUsuario(event: Event): void {
    event.preventDefault();
    if (this.edicionForm.valid) {
      this.empleadosService.updateEmpleado(this.empleadoID, this.edicionForm.value)
        .pipe(takeWhile(() => this.alive))
        .subscribe(() => {
          this.router.navigateByUrl('/');
        });
    }
  }

  ngOnDestroy(): void {
    this.alive = false;
  }

}
