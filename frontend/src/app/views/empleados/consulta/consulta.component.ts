import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { takeWhile } from 'rxjs/operators';
import { Area } from 'src/app/core/entities/area';
import { Empleado } from 'src/app/core/entities/empleado';
import { Pais } from 'src/app/core/entities/pais';
import { TipoIdentificacion } from 'src/app/core/entities/tipoIdentificacion';
import { AreasService } from 'src/app/core/services/areas/areas.service';
import { EmpleadosService } from 'src/app/core/services/empleados/empleados.service';
import { PaisesService } from 'src/app/core/services/paises/paises.service';
import { TiposIdentificacionService } from 'src/app/core/services/tiposIdentificacion/tipos-identificacion.service';

@Component({
  selector: 'app-consulta',
  templateUrl: './consulta.component.html',
  styleUrls: ['./consulta.component.scss']
})
export class ConsultaComponent implements OnInit, OnDestroy {

  alive = true;
  empleados: Empleado[] = [];
  areasKeys: { [id: string]: Area } = {};
  tiposIdentificacionKeys: { [id: string]: TipoIdentificacion } = {};
  paisesKeys: { [id: string]: Pais } = {};
  filtros: { [id: string]: string | null} = {};
  filtrosForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private empleadosService: EmpleadosService,
    private areasService: AreasService,
    private tiposIdentificacionService: TiposIdentificacionService,
    private paisesService: PaisesService,
  ) {
    this.filtrosForm = this.formBuilder.group({
      primerNombre: [null, []],
      otrosNombres: [null, []],
      primerApellido: [null, []],
      segundoApellido: [null, []],
      // fk_idPais: [null, []],
      // fk_idTipoIdentificacion: [null, []],
      // numeroIdentificacion: [null, []],
      // fechaDeIngresoTimestamp: [null, []],
      // correo: [null, []]
    });
  }

  ngOnInit(): void {
    this.retrieveEmpleados();
    this.retrieveAreas();
    this.retrievePaises();
    this.retrieveTiposIdentificacion();
  }

  retrieveEmpleados(): void {
    this.empleadosService.getEmpleados(this.filtros)
      .pipe(takeWhile(() => this.alive))
      .subscribe(empleados => {
        this.empleados = empleados;
    });
  }

  retrieveAreas(): void {
    this.areasService.getAreas()
      .pipe(takeWhile(() => this.alive))
      .subscribe((areas) => {
        this.areasKeys = {};
        areas.forEach(area => {
          this.areasKeys[area.idArea] = area;
        });
    });
  }

  retrievePaises(): void {
    this.paisesService.getPaises()
      .pipe(takeWhile(() => this.alive))
      .subscribe((paises) => {
        this.paisesKeys = {};
        paises.forEach(pais => {
          this.paisesKeys[pais.idPais] = pais;
        });
    });
  }

  retrieveTiposIdentificacion(): void {
    this.tiposIdentificacionService.getTiposIdentificacion()
      .pipe(takeWhile(() => this.alive))
      .subscribe((tiposIdentificacion) => {
        this.tiposIdentificacionKeys = {};
        tiposIdentificacion.forEach(tipoIdentificacion => {
          this.tiposIdentificacionKeys[tipoIdentificacion.idTipoIdentificacion] = tipoIdentificacion;
        });
    });
  }

  search(event: Event): void {
    event.preventDefault();
    Object.keys(this.filtrosForm.value).forEach(key => {
      this.filtros[key] = this.filtrosForm.value[key] || null;
    });
    this.retrieveEmpleados();
  }

  resetFiltros(event: Event): void {
    this.filtros = {};
    this.retrieveEmpleados();
  }

  deleteEmpleado(event: Event): void {
    event.preventDefault();
  }

  ngOnDestroy(): void {
    this.alive = false;
  }

}
