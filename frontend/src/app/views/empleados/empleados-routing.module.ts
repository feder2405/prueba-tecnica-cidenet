import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ConsultaComponent } from './consulta/consulta.component';
import { EdicionComponent } from './edicion/edicion.component';
import { RegistroComponent } from './registro/registro.component';

const routes: Routes = [{
  path: '', component: ConsultaComponent,
}, {
  path: 'editar/:idEmpleado', component: EdicionComponent,
}, {
  path: 'registrar', component: RegistroComponent,
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmpleadosRoutingModule { }
