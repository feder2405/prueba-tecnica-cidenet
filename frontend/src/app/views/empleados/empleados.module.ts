import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EmpleadosRoutingModule } from './empleados-routing.module';
import { RegistroComponent } from './registro/registro.component';
import { ConsultaComponent } from './consulta/consulta.component';
import { EdicionComponent } from './edicion/edicion.component';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [RegistroComponent, ConsultaComponent, EdicionComponent],
  imports: [
    CommonModule,
    EmpleadosRoutingModule,
    SharedModule
  ]
})
export class EmpleadosModule { }
