import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { GetReceiver } from '../../entities/get-receiver';
import { Pais } from '../../entities/pais';

@Injectable({
  providedIn: 'root'
})
export class PaisesService {

  headers = new HttpHeaders({
    Accept: 'application/json',
    'Content-Type': 'application/json',
  });

  constructor(
    private http: HttpClient
  ) { }

  getPaises(): Observable<Pais[]> {
    const url = `${environment.serverHost}paises`;
    return this.http.get<GetReceiver<Pais[]>>(url, {headers: this.headers})
      .pipe(map(response => response.data));
  }

}
