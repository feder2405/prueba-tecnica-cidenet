import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { GetReceiver } from '../../entities/get-receiver';
import { TipoIdentificacion } from '../../entities/tipoIdentificacion';

@Injectable({
  providedIn: 'root'
})
export class TiposIdentificacionService {

  headers = new HttpHeaders({
    Accept: 'application/json',
    'Content-Type': 'application/json',
  });

  constructor(
    private http: HttpClient
  ) { }

  getTiposIdentificacion(): Observable<TipoIdentificacion[]> {
    const url = `${environment.serverHost}tiposIdentificacion`;
    return this.http.get<GetReceiver<TipoIdentificacion[]>>(url, {headers: this.headers})
      .pipe(map(response => response.data));
  }

}
