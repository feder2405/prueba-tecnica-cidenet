import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Area } from '../../entities/area';
import { GetReceiver } from '../../entities/get-receiver';

@Injectable({
  providedIn: 'root'
})
export class AreasService {

  headers = new HttpHeaders({
    Accept: 'application/json',
    'Content-Type': 'application/json',
  });

  constructor(
    private http: HttpClient
  ) { }

  getAreas(): Observable<Area[]> {
    const url = `${environment.serverHost}areas`;
    return this.http.get<GetReceiver<Area[]>>(url, {headers: this.headers})
      .pipe(map(response => response.data));
  }

}
