import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Empleado } from '../../entities/empleado';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { GetReceiver } from '../../entities/get-receiver';
import { SearchEmpleado } from '../../entities/search-empleado';

@Injectable({
  providedIn: 'root'
})
export class EmpleadosService {

  headers = new HttpHeaders({
    Accept: 'application/json',
    'Content-Type': 'application/json',
  });

  constructor(
    private http: HttpClient
  ) { }

  getEmpleados(filtros: {[id: string]: string | null}): Observable<Empleado[]> {
    let url = `${environment.serverHost}empleados?`;
    const {primerNombre, otrosNombres, primerApellido, segundoApellido} = filtros;
    if (primerNombre) { url += `primerNombre=${primerNombre}&`; }
    if (otrosNombres) { url += `otrosNombres=${otrosNombres}&`; }
    if (primerApellido) { url += `primerApellido=${primerApellido}&`; }
    if (segundoApellido) { url += `segundoApellido=${segundoApellido}&`; }
    return this.http.get<GetReceiver<Empleado[]>>(url, {headers: this.headers})
      .pipe(map(response => response.data));
  }

  getEmpleado(empleadoID: number): Observable<Empleado> {
    const url = `${environment.serverHost}empleados/${empleadoID}`;
    return this.http.get<GetReceiver<Empleado[]>>(url, {headers: this.headers})
      .pipe(map(response => response.data[0]));
  }

  registrarEmpleado(empleado: Partial<Empleado>): Observable<void> {
    empleado = {...empleado, fechaDeIngreso: empleado.fechaDeIngresoTimestamp?.valueOf()};
    const url = `${environment.serverHost}empleados`;
    return this.http.post<void>(url, empleado, {headers: this.headers});
  }

  updateEmpleado(idEmpleado: number, empleado: Partial<Empleado>): Observable<void> {
    const url = `${environment.serverHost}empleados/${idEmpleado}`;
    return this.http.put<void>(url, empleado, {headers: this.headers});
  }
}
