export interface Empleado {
  idEmpleado: number;
  primerApellido: string;
  segundoApellido: string;
  primerNombre: string;
  otrosNombres: string;
  pais: string;
  tipoIdentificacion: string;
  numeroIdentificacion: string;
  correo: string;
  fechaDeIngreso: number;
  fechaDeIngresoTimestamp: Date;
  area: string;
  estado: string;
  horaRegistro: Date;
  fk_idTipoIdentificacion: number;
  fk_idPais: number;
  fk_idArea: number;
}
