export interface SearchEmpleado {
  primerNombre: string | null;
  otrosNombres: string | null;
  primerApellido: string | null;
  segundoApellido: string | null;
  // fk_idPais: number;
  // fk_idTipoIdentificacion: number;
  // numeroIdentificacion: string;
  // fechaDeIngresoTimestamp: Date;
  // fechaDeIngreso: number;
  // correo: string;
}
