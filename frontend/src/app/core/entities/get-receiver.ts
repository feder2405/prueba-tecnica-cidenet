export interface GetReceiver<T>{
  data: T;
  response: string;
  status: number;
}