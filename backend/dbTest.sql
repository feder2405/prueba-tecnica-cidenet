create database CidenetEmpleadosTest;
use CidenetEmpleadosTest;
create table Paises(
  idPais int primary key auto_increment,
  nombre varchar(30)
);
insert into Paises(idPais,nombre) values (1, "Colombia"), (2, "Estados Unidos");
create table TiposIdentificacion(
  idTipoIdentificacion int primary key auto_increment,
  nombre varchar(30)
);
insert into TiposIdentificacion(idTipoIdentificacion, nombre) values (1, "Cédula de Ciudadanía"), (2, "Cédula de Extranjería"), (3, "Pasaporte"), (4, "Permiso especial");
create table Areas (
  idArea int primary key auto_increment,
  nombre varchar(40)
);
insert into Areas(idArea, nombre) values 
  (1, "Administración"), (2, "Financiera"), (3, "Compras"), (4, "Infraestructura"), 
  (5, "Operación"), (6, "Talento Humano"), (7, "Servicios Varios");
create table Empleados(
  idEmpleado bigint primary key auto_increment,
  primerApellido varchar(20),
  segundoApellido varchar(20),
  primerNombre varchar(20),
  otrosNombres varchar(50),
  numeroIdentificacion varchar(20),
  correo varchar(300) unique key,
  fechaDeIngreso date,
  estado varchar(20),
  horaRegistro timestamp default current_timestamp,
  fk_idPais int,
  fk_idTipoIdentificacion int,
  fk_idArea int,
  constraint foreign key(fk_idPais) references Paises(idPais),
  constraint foreign key(fk_idTipoIdentificacion) references TiposIdentificacion(idTipoIdentificacion),
  constraint foreign key(fk_idArea) references Areas(idArea)
);

create table GeneradorCorreos(
  id int primary key auto_increment,
  correo varchar(300) unique key,
  contador int
);

-- create procedure proc_GenerarCorreo(empleadoID)

-- start transaction;
-- select @cont := contador from GeneradorCorreos where correo="fede@gmail.com" for update;
-- update Empleado set correo=concat(correo,@cont+1) where 
-- update GeneradorCorreos set contador = @cont + 1 where correo="fede@gmail.com";
-- commit;