jest.setTimeout(20000);
describe('filter users correctly', () => {
  let db;
  let empleadosService;
  let searchQueries;
  beforeAll(() => {
    db = require('../bin/database').init({db: 'CidenetEmpleadosTest'}); 
    empleadosService = require('../services/empleadosService')
                      .init({});
  })
  afterAll(() => {
    db.end();
  });
  searchQueries = [{
    name: 'empty search query',
    args: {},
    expected: {query: "select * from Empleados", params: []}
  },{
    name: 'primerNombre query',
    args: {primerNombre: 'juan'},
    expected: {query: "select * from Empleados where primerNombre like ?", params: ['%juan%']}
  },{
    name: 'allNombres query',
    args: {
      primerNombre: 'juan', otrosNombres: 'pablo d',
      primerApellido: 'rodriguez', segundoApellido: 'vasquez'
    },
    expected: {
      query: 'select * from Empleados where '
              + 'primerNombre like ? ' 
              + 'AND otrosNombres like ? ' 
              + 'AND primerApellido like ? ' 
              + 'AND segundoApellido like ?', 
      params: ['%juan%', '%pablo d%', '%rodriguez%', '%vasquez%']},
  },{
    name: 'allNombres empty',
    args: {
      primerNombre: '', otrosNombres: '', primerApellido: '', segundoApellido: ''
    },
    expected: {
      query: 'select * from Empleados', params: []},
  }];
  searchQueries.forEach(query => {
    test(query.name, () => {
      const result = empleadosService.buildSearchQuery(query.args);
      expect(result).toStrictEqual(query.expected);
      return db.execute(result.query, result.params);
    });
  });
});

describe('can register users', () => {
  let db;
  let empleadosService;
  beforeAll(async () => {
    db = require('../bin/database').init({db: 'CidenetEmpleadosTest'});
    const validationService = require('../services/validationService').init({});
    const correoService = require('../services/correoService').init({db});
    empleadosService = require('../services/empleadosService')
                      .init({db,correoService,validationService});
    await db.execute('delete from Empleados');
    await db.execute('delete from GeneradorCorreos');
  });
  afterAll(() => {
    db.end();
  })

  // const validationService = require('../services/validationService').init();
  // const correoService = require('../services/correoService').init({db: mockDbCorreo});
  // const empleadosService = require('../services/empleadosService')
  //   .init({db: mockDbRegister, validationService, correoService});
  test('10 emails with same name', async () => {
    const promises = []
    for(let i = 0; i < 10; ++i) {
      promises.push(empleadosService.registrarEmpleado({
        primerNombre: 'juan',
        otrosNombres: 'de la cruz',
        primerApellido: 'rodro',
        segundoApellido: 'gomez',
        numeroIdentificacion: '1234',
        fechaDeIngreso: Date.now() - 1 * 24 * 60 * 60 * 1000,
        fk_idPais: 1,
        fk_idTipoIdentificacion: 1,
        fk_idArea: 1
      }));
    }
    await Promise.all(promises);
  })
})