const { ValidationError } = require('joi');
let validationService;

beforeAll(() => {
  validationService = require('../services/validationService').init();
})

describe('validates register failure', () => {
  const empleadoCompleto = {
    primerNombre:'Juan', otrosNombres: 'Pablo', primerApellido: 'Rodriguez', segundoApellido: 'Lopez',
    numeroIdentificacion: '1234', fk_idPais: 1, fk_idTipoIdentificacion: 1, fk_idArea:1, fechaDeIngreso: Date.now()
  }
  const empleadosIncompletos = {
    base: { primerNombre: 'Juan' },
    empty: {},
    primNomNum: { ...empleadoCompleto, primerNombre: 2 },
    primNomNul: { ...empleadoCompleto, primerNombre: null },
    primNomEmp: { ...empleadoCompleto, primerNombre: '' }, 
    otroNomNum: { ...empleadoCompleto, otrosNombres: 2 },
    primApeNum: { ...empleadoCompleto, primerApellido: 2 },
    primApeNul: { ...empleadoCompleto, primerApellido: null },
    primApeEmp: { ...empleadoCompleto, primerApellido: '' },
    seguApeNum: { ...empleadoCompleto, segundoApellido: 2 },
    seguApeNul: { ...empleadoCompleto, segundoApellido: null },
    seguApeEmp: { ...empleadoCompleto, segundoApellido: '' },
    numIdNum: { ...empleadoCompleto, numeroIdentificacion: 2 },
    numIdNul: { ...empleadoCompleto, numeroIdentificacion: null },
    numIdEmp: { ...empleadoCompleto, numeroIdentificacion: '' },
    fkPaisNul: { ...empleadoCompleto, fk_idPais: null },
    fkPaisEmp: { ...empleadoCompleto, fk_idPais: '' },
    fkTipoIdNul: { ...empleadoCompleto, fk_idTipoIdentificacion: null },
    fkTipoIdEmp: { ...empleadoCompleto, fk_idTipoIdentificacion: '' },
    fkAreaNul: { ...empleadoCompleto, fk_idArea: null },
    fkAreaEmp: { ...empleadoCompleto, fk_idArea: '' },
    fechaIngNul: { ...empleadoCompleto, fechaDeIngreso: null },
    fechaIngEmp: { ...empleadoCompleto, fechaDeIngreso: '' },
    fechaIngTooOld: { ...empleadoCompleto, fechaDeIngreso: Date.now() - 32 * 24 * 60 * 60 * 1000 }, 
    fechaIngFuture: { ...empleadoCompleto, fechaDeIngreso: Date.now() + 1 * 24 * 60 * 60 * 1000 }, 
  }
  Object.keys(empleadosIncompletos).forEach((empleadoKey) => {
    const empleado = empleadosIncompletos[empleadoKey];
    test(`test empleado: ${empleadoKey}`, () => {
      expect(() => {validationService.validateRegisterEmpleado(empleado)})
        .toThrow(ValidationError);
    })
  })
});

describe('validates register success', () => {
  const empleadoCompleto = {
    primerNombre:'Juan', otrosNombres: 'Pablo', primerApellido: 'Rodriguez', segundoApellido: 'Lopez',
    numeroIdentificacion: '1234', fk_idPais: 1, fk_idTipoIdentificacion: 1, fk_idArea:1, fechaDeIngreso: Date.now()
  }
  test('', () => {
    validationService.validateRegisterEmpleado(empleadoCompleto);
  })
})