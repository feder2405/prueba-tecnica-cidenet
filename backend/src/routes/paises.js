const express = require('express');
const responseMessages = require('../utils/responseMessages');
const { generateResponseForError } = require('../utils/responseMessages');

function init ({ paisesService }) {
  const router = express.Router();
  router.get('/paises', async (req, res, next) => {
    paisesService.getPaises().then(data => {
      res.json({...responseMessages.OK, data});
    }).catch(err => {
      const response = generateResponseForError(err);
      res.status(response.status).json(response);
    })
  });
  return router;
}

module.exports = { init };