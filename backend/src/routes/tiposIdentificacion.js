const express = require('express');
const responseMessages = require('../utils/responseMessages');
const { generateResponseForError } = require('../utils/responseMessages');

function init ({ tiposIdentificacionService }) {
  const router = express.Router();
  router.get('/tiposIdentificacion', async (req, res, next) => {
    tiposIdentificacionService.getTiposIdentificacion().then(data => {
      res.json({...responseMessages.OK, data});
    }).catch(err => {
      const response = generateResponseForError(err);
      res.status(response.status).json(response);
    })
  });
  return router;
}


module.exports = { init };