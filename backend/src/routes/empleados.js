const express = require('express');
const { ValidationError } = require('joi');
const { generateResponseForError } = require('../utils/responseMessages');
const responseMessages = require('../utils/responseMessages');

function init ({ empleadosService }) {
  const router = express.Router();

  router.get('/empleados', async (req, res, next) => {
    empleadosService.getEmpleados(req.query).then(data => {
      res.json({...responseMessages.OK, data});
    }).catch(err => {
      const response = generateResponseForError(err);
      res.status(response.status).json(response);
    });
  });

  router.get('/empleados/:empleadoID', async (req, res, next) => {
    const { empleadoID } = req.params;
    empleadosService.getEmpleado(empleadoID).then(data => {
      res.json({...responseMessages.OK, data});
    }).catch(err => {
      const response = generateResponseForError(err);
      res.status(response.status).json(response);
    });
  })

  router.post('/empleados', async (req, res, next) => {
    empleadosService.registrarEmpleado(req.body).then(()=>{
      res.json(responseMessages.OK);
    }).catch(err => {
      const response = generateResponseForError(err);
      res.status(response.status).json(response);
    });
  });

  router.put('/empleados/:empleadoID', async (req, res, next) => {
    const { empleadoID } = req.params;
    empleadosService.actualizarEmpleado(empleadoID, req.body).then(() => {
      res.json(responseMessages.OK);
    }).catch(err => {
      const response = generateResponseForError(err);
      res.status(response.status).json(response);
    });
  })
  return router;
}


module.exports = { init };