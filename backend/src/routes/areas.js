const express = require('express');
const responseMessages = require('../utils/responseMessages');
const { generateResponseForError } = require('../utils/responseMessages');

function init ({ areasService }) {
  const router = express.Router();
  router.get('/areas', async (req, res, next) => {
    areasService.getAreas().then(data => {
      res.json({...responseMessages.OK, data});
    }).catch(err => {
      const response = generateResponseForError(err);
      res.status(response.status).json(response);
    })
  });
  return router;
}


module.exports = { 
  init
};