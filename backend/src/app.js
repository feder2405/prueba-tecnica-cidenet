require('dotenv').config();

var express = require('express');
var path = require('path');
var cors = require('cors');
var db = require('./bin/database').init();


require('express-async-errors');

var errorHandler = require('./middleware/errorHandler');
const validationService = require('./services/validationService').init();
const correoService = require('./services/correoService').init({db});
const empleadosService = require('./services/empleadosService').init({db, validationService, correoService});
const areasService = require('./services/areasService').init({db, validationService});
const paisesService = require('./services/paisesService').init({db, validationService});
const tiposIdentificacionService = require('./services/tiposIdentificacionService').init({db, validationService});
const empleadosRoute = require('./routes/empleados').init({ empleadosService });
const areasRoute = require('./routes/areas').init({ areasService });
const paisesRoute = require('./routes/paises').init({ paisesService });
const tiposIdentificacionRoute = require('./routes/tiposIdentificacion').init({ tiposIdentificacionService });

var app = express();
app.use(cors());
app.use(express.json());

app.use('/api/', empleadosRoute);
app.use('/api/', areasRoute);
app.use('/api/', paisesRoute);
app.use('/api/', tiposIdentificacionRoute);

app.use(errorHandler);

module.exports = app;