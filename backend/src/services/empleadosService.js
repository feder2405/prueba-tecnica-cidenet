// const mysql = require('mysql2/promise');
// mysql.createPool().execute("select * from Empleados").then((result) => {
//   console.log({r:result[0]});
// })

class EmpleadosService {
  constructor({db, validationService, correoService}) {
    this.db = db;
    this.validationService = validationService;
    this.correoService = correoService;
  }

  buildSearchQuery({primerNombre, otrosNombres, primerApellido, segundoApellido}){
    const where = primerNombre || otrosNombres || primerApellido || segundoApellido;
    let query = "select * from Empleados";
    let params = [];
    if (where) query += ' where ';
    let conditions = [];
    if (primerNombre) {
      conditions.push('primerNombre like ?');
      params.push('%' + primerNombre + '%');
    }
    if (otrosNombres) {
      conditions.push('otrosNombres like ?');
      params.push('%' + otrosNombres + '%');
    }
    if (primerApellido) {
      conditions.push('primerApellido like ?');
      params.push('%' + primerApellido + '%');
    }
    if (segundoApellido) {
      conditions.push('segundoApellido like ?');
      params.push('%' + segundoApellido + '%');
    }
    query = query + conditions.join(' AND ');
    return {query, params};
  }

  async getEmpleados({primerNombre, otrosNombres, primerApellido, segundoApellido}) {
    const {query, params} = this.buildSearchQuery({primerNombre, otrosNombres, primerApellido, segundoApellido});
    return (await this.db.execute(
      query, params
    ))[0];
  }

  async getEmpleado(idEmpleado) {
    return (await this.db.execute(
      "select * from Empleados where Empleados.idEmpleado = ?", [idEmpleado]
    ))[0];
  }

  async registrarEmpleado(empleado) {
    this.validationService.validateRegisterEmpleado(empleado);
    const {primerNombre, otrosNombres, primerApellido, segundoApellido,
      numeroIdentificacion, fechaDeIngreso, fk_idPais,
      fk_idTipoIdentificacion, fk_idArea} = empleado;
    const estado = 'Activo';
    const correo = await this.correoService.generarCorreo(empleado);
    await this.db.execute(
      "insert into Empleados(primerNombre, otrosNombres, primerApellido,"
      + " segundoApellido, numeroIdentificacion, fechaDeIngreso,"
      + " fk_idPais, fk_idTipoIdentificacion, fk_idArea, estado, correo)"
      + " values (?,?,?,?,?,?,?,?,?,?,?)",
      [primerNombre,otrosNombres || null,primerApellido,segundoApellido, 
        numeroIdentificacion, new Date(fechaDeIngreso), fk_idPais, 
        fk_idTipoIdentificacion, fk_idArea, estado, correo]
    );
  }

  async actualizarEmpleado(idEmpleado, empleado) {
    this.validationService.validateUpdateEmpleado({...empleado, idEmpleado});
    const {primerNombre, otrosNombres, primerApellido, segundoApellido,
      numeroIdentificacion, fk_idPais, fk_idTipoIdentificacion, fk_idArea} = empleado;
    const correo = await this.correoService.generarCorreo(empleado);
    await this.db.execute(
      "update Empleados set primerNombre = ?, otrosNombres = ?, primerApellido = ?,"
      + " segundoApellido = ?, numeroIdentificacion = ?,"
      + " fk_idPais = ?, fk_idTipoIdentificacion = ?, fk_idArea = ?, correo = ?"
      + " where idEmpleado = ?",
      [primerNombre,otrosNombres || null,primerApellido,segundoApellido, 
        numeroIdentificacion, fk_idPais, fk_idTipoIdentificacion, fk_idArea, 
        correo, idEmpleado]
    );
  }
}

function init(config) {
  return new EmpleadosService(config);
}

module.exports = {
  init
}