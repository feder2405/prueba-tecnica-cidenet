class AreasService {
  constructor({db, validationService}) {
    this.db = db;
    this.validationService = validationService;
  }

  async getAreas() {
    return (await this.db.execute(
      "select * from Areas"
    ))[0];
  }
}

function init(config) {
  return new AreasService(config);
}

module.exports = {
  init
}