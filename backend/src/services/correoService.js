// const ddd = require('../bin/database').init();
// ddd.getConnection().then(async conn => {
//   try{
//     await conn.beginTransaction()
//     const correo = '';
//     const contador = (await conn.execute(
//       'select contador from GeneradorCorreos where correo = ? for update', [correo]
//     ))[0][0].contador;
//     conn.execute('update GeneradorCorreos set contador = ?', [contador + 1]);
//     conn.commit();
//   } finally {
//     conn.release()
//   }
// })
class CorreoService {
  constructor({db}){
    this.db = db;
  }

  async getPais(idPais) {
    return [await this.db.execute('select * from Paises where idPais = ?', [idPais])][0][0][0];
  }

  async generarCorreo(empleado) {
    const pais = await this.getPais(empleado.fk_idPais);
    const tradPais = {
      Colombia: 'co',
      'Estados Unidos': 'us',
    }
    const conn = await this.db.getConnection();
    let contador = 1;
    try{
      await conn.beginTransaction()
      let correo = `${empleado.primerNombre}.${empleado.primerApellido}@cidenet.com.${tradPais[pais.nombre]}`;
      // correo = ''
      let response = (await conn.execute(
        'select contador from GeneradorCorreos where correo = ? for update', [correo]
      ))[0][0];
      console.log({response});
      if(response){
        contador = response.contador;
        await conn.execute('update GeneradorCorreos set contador = ?', [contador + 1]);
      } else {
        contador = 1;
        await conn.execute('insert into GeneradorCorreos(correo, contador) values (?, ?)', [correo, 2]);
      }
      await conn.commit();
    } catch(err){
      await conn.rollback();
      throw err; 
    }finally {
      await conn.release();
    }
    return `${empleado.primerNombre}.${empleado.primerApellido}.${contador}@cidenet.com.${tradPais[pais.nombre]}`;
  }
}

function init(config) {
  return new CorreoService(config);
}

module.exports = {
  init
}