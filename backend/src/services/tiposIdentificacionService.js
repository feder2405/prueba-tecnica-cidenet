class tiposIdentificacionService {
  constructor({db, validationService}) {
    this.db = db;
    this.validationService = validationService;
  }

  async getTiposIdentificacion() {
    return (await this.db.execute(
      "select * from TiposIdentificacion"
    ))[0];
  }
}

function init(config) {
  return new tiposIdentificacionService(config);
}

module.exports = {
  init
}