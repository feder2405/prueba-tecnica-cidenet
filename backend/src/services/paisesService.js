class PaisesService {
  constructor({db, validationService}) {
    this.db = db;
    this.validationService = validationService;
  }

  async getPaises() {
    return (await this.db.execute(
      "select * from Paises"
    ))[0];
  }
}

function init(config) {
  return new PaisesService(config);
}

module.exports = {
  init
}