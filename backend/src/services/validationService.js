const joi = require('joi');
class ValidationService{
  constructor() {

  }

  validateRegisterEmpleado(empleado) {
    const now = Date.now();
    const oneMonthAgo = now - 31 * 24 * 60 * 60 * 1000;
    const schema = joi.object().keys({
      primerApellido: joi.string().min(1).max(20).required(),
      segundoApellido: joi.string().min(1).max(20).required(),
      primerNombre: joi.string().min(1).max(20).required(),
      otrosNombres: joi.string().allow('').max(50).optional(),
      numeroIdentificacion: joi.string().min(1).max(20).required(),
      fechaDeIngreso: joi.number().min(oneMonthAgo).max(now).required(), 
      fk_idPais: joi.number().required(),
      fk_idTipoIdentificacion: joi.number().required(),
      fk_idArea: joi.number().required(),
    })
    const validation = schema.validate(empleado, {allowUnknown: true});
    if (validation.error) throw validation.error;
  }

  validateUpdateEmpleado(empleado) {
    const schema = joi.object().keys({
      idEmpleado: joi.number().required(),
      primerApellido: joi.string().min(1).max(20).required(),
      segundoApellido: joi.string().min(1).max(20).required(),
      primerNombre: joi.string().min(1).max(20).required(),
      otrosNombres: joi.string().max(50),
      numeroIdentificacion: joi.string().min(1).max(20).required(),
      fk_idPais: joi.number().required(),
      fk_idTipoIdentificacion: joi.number().required(),
      fk_idArea: joi.number().required(),
    })
    const validation = schema.validate(empleado, {allowUnknown: true});
    if (validation.error) throw validation.error;
  }
}

function init() {
  return new ValidationService();
}

module.exports = {
  init
}