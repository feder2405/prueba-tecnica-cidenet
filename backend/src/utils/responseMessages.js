const { ValidationError } = require("joi");

function generateResponseForError(err) {
  if (err instanceof ValidationError){
    return {
      status: 400,
      response: `INVALID_REQUEST: ${err.message}`,
    };
  } else {
    console.error({responseError: err});
    return {
      status: 500,
      response: 'SQL_ERROR'
    };
  }
}

module.exports = {
  OK: {
    status: 200,
    response: 'OK'
  },
  generateResponseForError,
  /*SQL_ERROR: {
    status: 500,
    response: 'NO_DATABASE_RESPONSE'
  },
  VALIDATION_ERROR: {
    status: 400,
    response: 'INVALID_REQUEST'
  }*/
}