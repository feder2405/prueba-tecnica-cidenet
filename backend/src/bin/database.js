const mysql = require('mysql2/promise');

function init (configs) {
  configs = configs || {};
  console.log({
    host: configs.host || process.env.CIDENET_MYSQL_HOST,
    user: configs.user || process.env.CIDENET_MYSQL_USER,
    database: configs.db || process.env.CIDENET_MYSQL_DB,
    password: configs.password || process.env.CIDENET_MYSQL_PASSWORD,
    
  })
  return mysql.createPool({
    host: configs.host || process.env.CIDENET_MYSQL_HOST,
    user: configs.user || process.env.CIDENET_MYSQL_USER,
    database: configs.db || process.env.CIDENET_MYSQL_DB,
    password: configs.password || process.env.CIDENET_MYSQL_PASSWORD,
    
  });
};

module.exports = {
  init,
} 